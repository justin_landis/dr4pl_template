dr4pl template guide
====================

Clone this Code!
----------------

Before we begin, you need to clone this code. Visit the following site through your atlassian account: <https://bitbucket.org/justin_landis/dr4pl_template/src/master/> The web address you need may not be exactly this one. Click on clone in the top right of the webpage. ![](images/Bitbucket_clone.png) You can either use command line as directed in this pop up to clone the repository to your working directory. Or you can copy the web address and use Rstudio to clone the version control repository. Select **File** -&gt; **New Project...** ![](images/Clone_step1.png) ![](images/Clone_step2.png) ![](images/Clone_step3.png)

And paste the web address in the repository URL: and let other feilds auto fill. Finially select Create Project. You will be prompted for a bitbucket username and password to clone. This repository should be open for general-lab Users.

Script
======

### Required packages

These packages are used within the script and will need to be installed prior to running the script. To install these packages click the follwoing in this order: **Packages** -&gt; **install** -&gt;

![](images/install_packages.png)

``` r
library(ggplot2)
library(dr4pl)
library(readxl)
```

--------------------------------------------------------------------------------------
======================================================================================

Reading Data sets into R
------------------------

With R, you have many options of reading data. The format you choose to save your data matters because it will determine which function you need to read the data. I have two example files in my Data directory. One file is csv, the other is xlsx

| csv                             | xlsx                             |
|---------------------------------|----------------------------------|
| ![](images/head_examplecsv.png) | ![](images/head_examplexlsx.png) |

``` r
#Reading Data sets
data1 <- read.csv(file = "Data/example_csv.csv", header = TRUE)             #clips first entry for some reason
head(data1)
```

    ##   Well.Row Well.Col   Content Raw.Data..lens.
    ## 1        A        2 Sample X2           857.5
    ## 2        A        3 Sample X3         27780.9
    ## 3        A        4 Sample X4         86036.3
    ## 4        A        5 Sample X5         99393.7
    ## 5        A        6 Sample X6         99691.2
    ## 6        A        7 Sample X7        110783.4

``` r
data2 <- readxl::read_xlsx("Data/example_xlsx.xlsx", sheet = "Sheet2")      #recommended file reader
head(data2)
```

    ## # A tibble: 6 x 4
    ##   `Well\r\nRow` `Well\r\nCol` Content   `Raw Data (lens)`
    ##   <chr>                 <dbl> <chr>                 <dbl>
    ## 1 A                        1. Sample X1              14.1
    ## 2 A                        2. Sample X2              94.3
    ## 3 A                        3. Sample X3             804. 
    ## 4 A                        4. Sample X4            2120. 
    ## 5 A                        5. Sample X5            1709. 
    ## 6 A                        6. Sample X6            2375.

This is a good reason why you must check your data before and after importing into R. Just to make sure that your data is not missread. Since data2 was read in correclty, I will continue the code using this data.

This portion of the code is for you to mainually enter the concentrations of your dilutions. later we should Double check that the number matches our experimental design in our data.

``` r
conc <- c(500,50,5,0.5,0.05,0.005,0.0005,0.00005,0.000005,0.0000005)
```

This renames the column names of the data. This step is important to make latter parts of the code consistent.

``` r
colnames(data2) <- c("Well.Row","Well.Col","Content","Response")
```

--------------------------------------------------------------------------------------
======================================================================================

Scaling
-------

This portion of the code is optional and is 'commented out'. Anything following the \# symbol will not be read in the code. Comments are handy tools to leave notes and a useful debugging tool if you decide to expand on this template code.

``` r
#Scaling each set to 0-100
#data2$Response <- data2$Response - min(data2$Response)
#data2$Response <- data2$Response/(max(data2$Response))*100
```

--------------------------------------------------------------------------------------
======================================================================================

subsetting code
---------------

Here we start to subset the code. Our first round of subsetting is removing the control samples from our data because they were not a part of the serial dilutions. In this example, well column numbers 11 and 12 were controls. The first line may be read as the following: "subset data2 such that Well.Col does NOT contain values 11 and 12, and then save this to data2" The next line saves a new column named "Dose" and puts conc vector into it

``` r
#Remove control & Add concentration to Data AC
data2 <- subset(data2, !Well.Col %in% c(11,12))   # First
data2$Dose <- conc                                # Second
head(data2)
```

    ## # A tibble: 6 x 5
    ##   Well.Row Well.Col Content   Response      Dose
    ##   <chr>       <dbl> <chr>        <dbl>     <dbl>
    ## 1 A              1. Sample X1     14.1 500.     
    ## 2 A              2. Sample X2     94.3  50.0    
    ## 3 A              3. Sample X3    804.    5.00   
    ## 4 A              4. Sample X4   2120.    0.500  
    ## 5 A              5. Sample X5   1709.    0.0500 
    ## 6 A              6. Sample X6   2375.    0.00500

In your experiments, you data may contain different treatments that you want to plot separate curves for. In this example lets say there were two treatments. Those in Row A-D were treatment1 and E-H were treatment2. You can interpret the code as the following: "subset data2 such that Well.Row contains c("A","B","C","D"), and then save this to data2.treatment1" and so on for the next line of code.

``` r
#Split cell types into two data frames
data2.treatment1 <- subset(data2, Well.Row %in% c("A","B","C","D"))
data2.treatment2 <- subset(data2, Well.Row %in% c("E","F","G","H"))
```

--------------------------------------------------------------------------------------
======================================================================================

Using dr4pl
-----------

This variable may be assigned some string to help you keep track of what you are plotting

``` r
#Drug name for plot
drug <- "MyCoolDrug"
```

This line runs the dr4pl code on your data and approximates values for the 4 parameter logistic model. There are two addition arguments that may be used if you are unsatisfied with your end graph. method.init and method.robust. See the dr4pl help page if you wish to try approximation methods.

``` r
#creating dr4pl object
dr4pl.obj1 <- dr4pl(Response~Dose, data = data2.treatment1)
```

This assigns the IC50 value as a character string to be used in our graph in the following lines.

``` r
IC50 <- paste("IC50 =",round(digits = 2, dr4pl.obj1$parameters[2]))
```

At the most basic form, you could use plot(dr4pl.obj) to get our default graph. The following code shows modifications on that default graph. text.title controls the title of the graph. text.x controls the text for the x-axisis. geom\_text is a function to append text to a graph, here is where we specify the IC50 value.

``` r
plot(dr4pl.obj1, text.title = paste(drug, "treatment1 IC50 plot", sep = " "), text.x = "conc. (uM)") +
  geom_text(
    aes(label = IC50, y = 20 , x= 0.200),
    position = position_dodge(0.9),
    vjust = 0
  )
```

![](dr4pl_template_guide_files/figure-markdown_github/unnamed-chunk-12-1.png)

Finially we save the graph with the ggsave function. Helpful tip, do not use spaces in file names, use the \_ character instead.

``` r
ggsave(paste(drug, "treatment1_IC50_plot.png", sep = "_"))
```

    ## Saving 7 x 5 in image

Here is the same code, but in one chunk of treatment2

``` r
dr4pl.obj2 <- dr4pl(Response~Dose, data = data2.treatment2)
IC50 <- paste("IC50 =",round(digits = 2, dr4pl.obj2$parameters[2]))
plot(dr4pl.obj2, text.title = paste(drug, "treatment2 IC50 plot", sep = " "), text.x = "conc. (uM)") +
  geom_text(
    aes(label = IC50, y = 20 , x= 0.200),
    position = position_dodge(0.9),
    vjust = 0
  )
```

![](dr4pl_template_guide_files/figure-markdown_github/unnamed-chunk-14-1.png)

``` r
ggsave(paste(drug, "treatment2_IC50_plot.png", sep = "_"))
```

    ## Saving 7 x 5 in image

Tips and Tricks
===============

When editing this template, use the Find and replace tool to help relabel variables. This will help keep the code consistent and minimize errors.
